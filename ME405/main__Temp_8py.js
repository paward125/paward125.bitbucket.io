var main__Temp_8py =
[
    [ "adc", "main__Temp_8py.html#a08425bc9cf26a62f5cb6bef7eb98c088", null ],
    [ "adi", "main__Temp_8py.html#a225a45fc18fc402e223a487588e8827e", null ],
    [ "blinko", "main__Temp_8py.html#a80880eb95ef495974218c3ea6a35e046", null ],
    [ "i", "main__Temp_8py.html#a099b769568c14a2917bc593d987ffbd7", null ],
    [ "i2c", "main__Temp_8py.html#a245324bcd839d5ae549dc72657d0bb02", null ],
    [ "internal_temp", "main__Temp_8py.html#ab589dd5536948e168437ed7da86e36a9", null ],
    [ "mcp", "main__Temp_8py.html#acce7864b047ca951dbd98c5edef1b599", null ],
    [ "sensor_C_val", "main__Temp_8py.html#a4d8a4d3fa475f4eb360cba3aa92cdc87", null ],
    [ "sensor_F_val", "main__Temp_8py.html#a057de5d29fb880edbad1274caefe9b27", null ],
    [ "time", "main__Temp_8py.html#a8b341954bc110ac84bd96b2eec2909e3", null ],
    [ "time_val", "main__Temp_8py.html#a68cd9eb90a0f1fdbc6e820061e4dcc5a", null ],
    [ "timeout", "main__Temp_8py.html#ae58b150219ff3fcb85eb7a599f9f7817", null ],
    [ "val", "main__Temp_8py.html#a017c5c81e006f80668183a1ae9386a62", null ]
];