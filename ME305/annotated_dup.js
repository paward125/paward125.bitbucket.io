var annotated_dup =
[
    [ "Backend", null, [
      [ "TaskData", "classBackend_1_1TaskData.html", "classBackend_1_1TaskData" ]
    ] ],
    [ "BLEdriver", null, [
      [ "BLEdriver", "classBLEdriver_1_1BLEdriver.html", "classBLEdriver_1_1BLEdriver" ]
    ] ],
    [ "BLEUI", null, [
      [ "TaskUI", "classBLEUI_1_1TaskUI.html", "classBLEUI_1_1TaskUI" ]
    ] ],
    [ "Blink", null, [
      [ "BlinkTask", "classBlink_1_1BlinkTask.html", "classBlink_1_1BlinkTask" ]
    ] ],
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask", null, [
      [ "ControllerTask", "classControllerTask_1_1ControllerTask.html", "classControllerTask_1_1ControllerTask" ]
    ] ],
    [ "Cooperative_Multitasking", null, [
      [ "Button", "classCooperative__Multitasking_1_1Button.html", "classCooperative__Multitasking_1_1Button" ],
      [ "MotorDriver", "classCooperative__Multitasking_1_1MotorDriver.html", "classCooperative__Multitasking_1_1MotorDriver" ],
      [ "TaskElevator", "classCooperative__Multitasking_1_1TaskElevator.html", "classCooperative__Multitasking_1_1TaskElevator" ]
    ] ],
    [ "Encoder", null, [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Lab7_Backend", null, [
      [ "TaskData", "classLab7__Backend_1_1TaskData.html", "classLab7__Backend_1_1TaskData" ]
    ] ],
    [ "Lab7_ClosedLoop", null, [
      [ "ClosedLoop", "classLab7__ClosedLoop_1_1ClosedLoop.html", "classLab7__ClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "Lab7_ControllerTask", null, [
      [ "ControllerTask", "classLab7__ControllerTask_1_1ControllerTask.html", "classLab7__ControllerTask_1_1ControllerTask" ]
    ] ],
    [ "LEDtime", null, [
      [ "TaskLEDblink", "classLEDtime_1_1TaskLEDblink.html", "classLEDtime_1_1TaskLEDblink" ],
      [ "TaskLEDteeth", "classLEDtime_1_1TaskLEDteeth.html", "classLEDtime_1_1TaskLEDteeth" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Task", null, [
      [ "TaskData", "classTask_1_1TaskData.html", "classTask_1_1TaskData" ]
    ] ],
    [ "TaskEncoder", null, [
      [ "TaskEncoder", "classTaskEncoder_1_1TaskEncoder.html", "classTaskEncoder_1_1TaskEncoder" ]
    ] ],
    [ "TaskUser", null, [
      [ "TaskUser", "classTaskUser_1_1TaskUser.html", "classTaskUser_1_1TaskUser" ]
    ] ]
];