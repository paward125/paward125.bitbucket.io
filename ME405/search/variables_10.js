var searchData=
[
  ['sensor_5fc_5fval_358',['sensor_C_val',['../main__Temp_8py.html#a4d8a4d3fa475f4eb360cba3aa92cdc87',1,'main_Temp']]],
  ['sensor_5ff_5fval_359',['sensor_F_val',['../main__Temp_8py.html#a057de5d29fb880edbad1274caefe9b27',1,'main_Temp']]],
  ['ser_360',['ser',['../Frontend_8py.html#a37ef4d955cea14b4a9c65a17da8302ce',1,'Frontend']]],
  ['ser_5fnum_361',['ser_num',['../classtask__share_1_1Queue.html#a6f9d87b116eb16dba0867d3746af9f5f',1,'task_share.Queue.ser_num()'],['../classtask__share_1_1Share.html#a2e8df029af46fbfd44ef0c2e7e8c7af6',1,'task_share.Share.ser_num()']]],
  ['share_5flist_362',['share_list',['../task__share_8py.html#a75818e5b662453e3723d0f234c85e519',1,'task_share']]],
  ['start_363',['START',['../proj_8py.html#a4a100742ac262e014147ac7aa9a2ae8e',1,'proj.START()'],['../TouchDriver_8py.html#a8b05ca676f0a3deb2549c1eed9f0a789',1,'TouchDriver.start()']]],
  ['start_5ftime_364',['start_time',['../TouchPad__Driver_8py.html#abc5e7e42549d8f679d437a45c7702a5e',1,'TouchPad_Driver']]],
  ['starttime_365',['startTime',['../proj_8py.html#a4c883b018367234befd61ad47c76fc12',1,'proj']]],
  ['state_366',['state',['../Vendotron_8py.html#afb66cd8b51cbd9ad39bfc7d7571b0819',1,'Vendotron']]]
];
