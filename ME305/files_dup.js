var files_dup =
[
    [ "Backend.py", "Backend_8py.html", [
      [ "TaskData", "classBackend_1_1TaskData.html", "classBackend_1_1TaskData" ]
    ] ],
    [ "BLE_main.py", "BLE__main_8py.html", "BLE__main_8py" ],
    [ "BLEdriver.py", "BLEdriver_8py.html", [
      [ "BLEdriver", "classBLEdriver_1_1BLEdriver.html", "classBLEdriver_1_1BLEdriver" ]
    ] ],
    [ "BLEUI.py", "BLEUI_8py.html", [
      [ "TaskUI", "classBLEUI_1_1TaskUI.html", "classBLEUI_1_1TaskUI" ]
    ] ],
    [ "Blink.py", "Blink_8py.html", [
      [ "BlinkTask", "classBlink_1_1BlinkTask.html", "classBlink_1_1BlinkTask" ]
    ] ],
    [ "ClosedLoop.py", "ClosedLoop_8py.html", [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask.py", "ControllerTask_8py.html", [
      [ "ControllerTask", "classControllerTask_1_1ControllerTask.html", "classControllerTask_1_1ControllerTask" ]
    ] ],
    [ "Cooperative_Multitasking.py", "Cooperative__Multitasking_8py.html", [
      [ "TaskElevator", "classCooperative__Multitasking_1_1TaskElevator.html", "classCooperative__Multitasking_1_1TaskElevator" ],
      [ "Button", "classCooperative__Multitasking_1_1Button.html", "classCooperative__Multitasking_1_1Button" ],
      [ "MotorDriver", "classCooperative__Multitasking_1_1MotorDriver.html", "classCooperative__Multitasking_1_1MotorDriver" ]
    ] ],
    [ "elevatorMain.py", "elevatorMain_8py.html", "elevatorMain_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "EncoderUserInterfaceMain.py", "EncoderUserInterfaceMain_8py.html", "EncoderUserInterfaceMain_8py" ],
    [ "Fibonacci.py", "Fibonacci_8py.html", "Fibonacci_8py" ],
    [ "Frontend.py", "Frontend_8py.html", "Frontend_8py" ],
    [ "Lab06main.py", "Lab06main_8py.html", "Lab06main_8py" ],
    [ "Lab7_Backend.py", "Lab7__Backend_8py.html", [
      [ "TaskData", "classLab7__Backend_1_1TaskData.html", "classLab7__Backend_1_1TaskData" ]
    ] ],
    [ "Lab7_ClosedLoop.py", "Lab7__ClosedLoop_8py.html", [
      [ "ClosedLoop", "classLab7__ClosedLoop_1_1ClosedLoop.html", "classLab7__ClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "Lab7_ControllerTask.py", "Lab7__ControllerTask_8py.html", [
      [ "ControllerTask", "classLab7__ControllerTask_1_1ControllerTask.html", "classLab7__ControllerTask_1_1ControllerTask" ]
    ] ],
    [ "Lab7_Frontend.py", "Lab7__Frontend_8py.html", "Lab7__Frontend_8py" ],
    [ "Lab7_main.py", "Lab7__main_8py.html", "Lab7__main_8py" ],
    [ "LED_main.py", "LED__main_8py.html", "LED__main_8py" ],
    [ "LEDtime.py", "LEDtime_8py.html", [
      [ "TaskLEDblink", "classLEDtime_1_1TaskLEDblink.html", "classLEDtime_1_1TaskLEDblink" ],
      [ "TaskLEDteeth", "classLEDtime_1_1TaskLEDteeth.html", "classLEDtime_1_1TaskLEDteeth" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "shar.py", "shar_8py.html", "shar_8py" ],
    [ "share.py", "share_8py.html", "share_8py" ],
    [ "Shared.py", "Shared_8py.html", "Shared_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "Task.py", "Task_8py.html", [
      [ "TaskData", "classTask_1_1TaskData.html", "classTask_1_1TaskData" ]
    ] ],
    [ "TaskEncoder.py", "TaskEncoder_8py.html", [
      [ "TaskEncoder", "classTaskEncoder_1_1TaskEncoder.html", "classTaskEncoder_1_1TaskEncoder" ]
    ] ],
    [ "TaskUser.py", "TaskUser_8py.html", [
      [ "TaskUser", "classTaskUser_1_1TaskUser.html", "classTaskUser_1_1TaskUser" ]
    ] ],
    [ "User.py", "User_8py.html", "User_8py" ]
];