var classTouchPad__Driver_1_1TouchDriver =
[
    [ "__init__", "classTouchPad__Driver_1_1TouchDriver.html#a18136d1481de4242fb4ccfedc3d92b6b", null ],
    [ "three_comp", "classTouchPad__Driver_1_1TouchDriver.html#ada0889f5f0f9110846bc0e7f44037b1e", null ],
    [ "Xread", "classTouchPad__Driver_1_1TouchDriver.html#a3de70d869fe2f40053c14f0025b88607", null ],
    [ "Yread", "classTouchPad__Driver_1_1TouchDriver.html#a2651c7a87e66dc5c3b7521baf30dd934", null ],
    [ "Zread", "classTouchPad__Driver_1_1TouchDriver.html#aa8b818f5671eb858bc1814bd34dcf4bf", null ],
    [ "adc_x_high", "classTouchPad__Driver_1_1TouchDriver.html#a20d728888636a5c4e33a7db6743f8fe5", null ],
    [ "adc_x_low", "classTouchPad__Driver_1_1TouchDriver.html#a199cc8995062a0577ff60974b7fd7938", null ],
    [ "adc_y_high", "classTouchPad__Driver_1_1TouchDriver.html#a8e988b8b8c1751e711a013e6c89f9060", null ],
    [ "adc_y_low", "classTouchPad__Driver_1_1TouchDriver.html#a949b87a38d5a0676018d8040492ea91e", null ],
    [ "center_X", "classTouchPad__Driver_1_1TouchDriver.html#a7745994ad1547112ba89341225a27a5a", null ],
    [ "center_Y", "classTouchPad__Driver_1_1TouchDriver.html#a10c71ab74e5386f3f038e33093e8b32f", null ],
    [ "length", "classTouchPad__Driver_1_1TouchDriver.html#aebd6c0fcc3db5d96f5bb1cf5cf6f8db2", null ],
    [ "mm_value_x", "classTouchPad__Driver_1_1TouchDriver.html#a8e8736f0d2d50fd1756e98f6b4ded19a", null ],
    [ "mm_value_y", "classTouchPad__Driver_1_1TouchDriver.html#a21f52ca80d72ea5b95fa4abc70724995", null ],
    [ "width", "classTouchPad__Driver_1_1TouchDriver.html#a2eb99b26fec548cb43c6aef8426ebbba", null ],
    [ "x_inc", "classTouchPad__Driver_1_1TouchDriver.html#a000199676c22ec95bc45244cc27be8c8", null ],
    [ "x_m", "classTouchPad__Driver_1_1TouchDriver.html#a3248ddd793e6d32b8a74deca2288c7a8", null ],
    [ "x_p", "classTouchPad__Driver_1_1TouchDriver.html#a6ab298f3adf7fb3934ff002aa7fcddd4", null ],
    [ "y_inc", "classTouchPad__Driver_1_1TouchDriver.html#a6f2717c3b970228f3ef290be1f4aa56c", null ],
    [ "y_m", "classTouchPad__Driver_1_1TouchDriver.html#af18caf94275135b5d2daef65329d46e4", null ],
    [ "y_p", "classTouchPad__Driver_1_1TouchDriver.html#a64a2f07338cd057d92d116431d966d0f", null ]
];