var classMotor__Driver_1_1MotorDriver =
[
    [ "__init__", "classMotor__Driver_1_1MotorDriver.html#a334787688f6b9c3ab9c046135a64db0d", null ],
    [ "disable", "classMotor__Driver_1_1MotorDriver.html#abd4507fc3fdac3fc9a10b2d6fa7c6608", null ],
    [ "enable", "classMotor__Driver_1_1MotorDriver.html#af6576759bd1844dc919e0f3e7ca9c1f9", null ],
    [ "nfault_handle", "classMotor__Driver_1_1MotorDriver.html#ac6dfc8a00eb8bb5e88a8cf996af80ee1", null ],
    [ "reboot", "classMotor__Driver_1_1MotorDriver.html#a95995edf5477bd9884ea00b773887dff", null ],
    [ "set_duty", "classMotor__Driver_1_1MotorDriver.html#ac7a0948738cdf2f79955042373d80dbf", null ],
    [ "extint", "classMotor__Driver_1_1MotorDriver.html#a7f27d047f2e8558ffbe9bf6e280d48a2", null ],
    [ "extint1", "classMotor__Driver_1_1MotorDriver.html#a689bbcbc74ed317f8e28fe33e019d410", null ],
    [ "IN1_pin", "classMotor__Driver_1_1MotorDriver.html#af40193d707228920a8dfe6a9c7ee3ec3", null ],
    [ "IN2_pin", "classMotor__Driver_1_1MotorDriver.html#a0b0aacd4e0cac3ac21f48e893c245639", null ],
    [ "IN3_pin", "classMotor__Driver_1_1MotorDriver.html#a2f5f997f30cd6d9e45e919b5e923f94d", null ],
    [ "IN4_pin", "classMotor__Driver_1_1MotorDriver.html#a163c7a5e970b7bca998760b2693e4ce1", null ],
    [ "min_dut", "classMotor__Driver_1_1MotorDriver.html#af5a139d94e17ee753bec728eb24ff329", null ],
    [ "nSLEEP_pin", "classMotor__Driver_1_1MotorDriver.html#a2b143d8e45f3e81006cdedb56ec676ca", null ],
    [ "t3ch1", "classMotor__Driver_1_1MotorDriver.html#a2776806218ef892ee23da7628fde2138", null ],
    [ "t3ch2", "classMotor__Driver_1_1MotorDriver.html#a4cc798ef27d57933de437e7f15d92f8f", null ],
    [ "t3ch3", "classMotor__Driver_1_1MotorDriver.html#a9714a9529b3fdeca8770e1c9c4458b99", null ],
    [ "t3ch4", "classMotor__Driver_1_1MotorDriver.html#aeabd43141ea116cd5e4c599927dc39c1", null ],
    [ "tim", "classMotor__Driver_1_1MotorDriver.html#a62a00b3480ed47b1e7b0a1f712dfcab6", null ],
    [ "Timer", "classMotor__Driver_1_1MotorDriver.html#a23cb51bab4320c0e7b8037862c730dbe", null ]
];