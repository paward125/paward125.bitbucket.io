var searchData=
[
  ['x_188',['X',['../classTouchDriver_1_1TouchDriver.html#a896a0a7f39137ee0a10103f548ea255c',1,'TouchDriver.TouchDriver.X()'],['../proj_8py.html#ac80fcaae4190a53ee63b5ad077518f36',1,'proj.x()']]],
  ['x_5finc_189',['x_inc',['../classTouchPad__Driver_1_1TouchDriver.html#a000199676c22ec95bc45244cc27be8c8',1,'TouchPad_Driver::TouchDriver']]],
  ['x_5fm_190',['x_m',['../classTouchPad__Driver_1_1TouchDriver.html#a3248ddd793e6d32b8a74deca2288c7a8',1,'TouchPad_Driver::TouchDriver']]],
  ['x_5fp_191',['x_p',['../classTouchPad__Driver_1_1TouchDriver.html#a6ab298f3adf7fb3934ff002aa7fcddd4',1,'TouchPad_Driver::TouchDriver']]],
  ['xaxis_5ftask_192',['Xaxis_task',['../proj_8py.html#a20d21c5cc5b8e465879d65a3332f80f0',1,'proj']]],
  ['xd_193',['xd',['../proj_8py.html#a8d81529824d2fb12d73374abafee50d6',1,'proj']]],
  ['xm_194',['xm',['../classTouchDriver_1_1TouchDriver.html#ad2bcfb9458b3796a969dd2e2b64b56d9',1,'TouchDriver.TouchDriver.xm()'],['../TouchDriver_8py.html#a676528f7c3953d19d80d7e26acfe7a91',1,'TouchDriver.xm()']]],
  ['xnot_195',['xnot',['../classTouchDriver_1_1TouchDriver.html#a9c2a4cfe7f5d2e8b80635dee7eb969f6',1,'TouchDriver::TouchDriver']]],
  ['xp_196',['xp',['../classTouchDriver_1_1TouchDriver.html#a761d121772a8eec2757a1cd4e2933291',1,'TouchDriver.TouchDriver.xp()'],['../TouchDriver_8py.html#aad8dc565e10728263253ceb689ad4e2e',1,'TouchDriver.xp()']]],
  ['xread_197',['Xread',['../classTouchPad__Driver_1_1TouchDriver.html#a3de70d869fe2f40053c14f0025b88607',1,'TouchPad_Driver::TouchDriver']]]
];
