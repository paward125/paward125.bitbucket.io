var classTouchDriver_1_1TouchDriver =
[
    [ "__init__", "classTouchDriver_1_1TouchDriver.html#a71a7e68e05484bfd4abb35be92fa6d0d", null ],
    [ "total", "classTouchDriver_1_1TouchDriver.html#a9c1683c7060feaa82e62bf9da97d5795", null ],
    [ "X", "classTouchDriver_1_1TouchDriver.html#a896a0a7f39137ee0a10103f548ea255c", null ],
    [ "Y", "classTouchDriver_1_1TouchDriver.html#ad1bd44e04a4d974900a0558bc45b5bc5", null ],
    [ "Z", "classTouchDriver_1_1TouchDriver.html#a7ea193a322713c71f18b155420d20f88", null ],
    [ "convx", "classTouchDriver_1_1TouchDriver.html#a8f3a3c3dc529c058c9836484a1be9672", null ],
    [ "convy", "classTouchDriver_1_1TouchDriver.html#ad49895884133ca748b7b3c3ecc67d07f", null ],
    [ "xm", "classTouchDriver_1_1TouchDriver.html#ad2bcfb9458b3796a969dd2e2b64b56d9", null ],
    [ "xnot", "classTouchDriver_1_1TouchDriver.html#a9c2a4cfe7f5d2e8b80635dee7eb969f6", null ],
    [ "xp", "classTouchDriver_1_1TouchDriver.html#a761d121772a8eec2757a1cd4e2933291", null ],
    [ "ym", "classTouchDriver_1_1TouchDriver.html#a37563134ee4a571827b3ceab6388f509", null ],
    [ "ynot", "classTouchDriver_1_1TouchDriver.html#a7404a979920a37b8d98f2426f727dedb", null ],
    [ "yp", "classTouchDriver_1_1TouchDriver.html#a62d7de472b167f08cd66899753297254", null ]
];